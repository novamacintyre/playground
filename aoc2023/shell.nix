with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "playground-aoc2023-dev";
  buildInputs = [
    (pkgs.buildFHSUserEnv {
      name = "_playground-aoc2023-dev_FHSUserEnv";
      targetPkgs = pkgs: (with pkgs; [
        python310Full
      ]);
    })
    python310Full
    python310Packages.ipython
    zlib
  ];
  shellHook = ''
    export LD_LIBRARY_PATH="${pkgs.zlib}/lib"
    if [[ ! -e .venv ]]; then
      python -m venv .venv/aoc2023
    fi
    source .venv/aoc2023/bin/activate
  '';
}
