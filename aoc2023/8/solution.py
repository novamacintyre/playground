import itertools
import math
import re

with open("puzzle_input", "r") as fo:
    puzzle_input = [line.strip() for line in fo.readlines()]

inst = puzzle_input[0]
nw = puzzle_input[2:]


class NetworkNode:

    _dir_names = ("L", "R")
    _string_repr_re = re.compile(r"^(\w{3})\s=\s\((\w{3}),\s(\w{3})\)$")

    def __init__(self, node_name, left_node, right_node):
        self.node_name = node_name
        self.left = left_node
        self.right = right_node

    def __str__(self):
        return self.node_name

    def __repr__(self):
        return self.node_name, self.left, self.right

    def __hash__(self):
        return hash(repr(self))

    @classmethod
    def from_string(cls, string):
        match = cls._string_repr_re.match(string)
        if not match:
            raise Exception(f"Could not match {string=} to expected format")
        return cls(match.group(1), match.group(2), match.group(3))


class Network:
    def __init__(self, network):
        self._network = {}
        for raw_node in network:
            node = NetworkNode.from_string(raw_node)
            self._network[node.node_name] = node

    def walk(self, node_name: str, direction: str) -> str:
        node = self._network[node_name]
        if direction == "L":
            return node.left
        return node.right

    def make_path(
        self,
        start_node: str,
        direction: str,
        stop_node: [str | None] = None
    ) -> list[str]:
        nodes = [start_node]
        new_node = start_node
        cycle = itertools.cycle(direction)
        while new_node != stop_node:
            d = next(cycle)
            new_node = self.walk(new_node, d)
            nodes.append(new_node)
        return nodes


network = Network(nw)
path = network.make_path("AAA", inst, "ZZZ")

print(len(path) - 1)


# PART II

class GhostNetwork(Network):

    def get_nodes_by_pattern(self, pattern: re.Pattern) -> set[str]:
        nodes = set()
        for n in self._network:
            if pattern.match(n):
                nodes.add(n)
        return nodes

    def make_cycle(
        self,
        start_node: str,
        direction: str,
        terminal_nodes: set[str]
    ) -> list[str]:
        # Returns current node and the next direction to take
        cycle = itertools.cycle(direction)
        d = next(cycle)
        nodes = [start_node]
        new_node = start_node
        while True:
            new_node = self.walk(new_node, d)
            d = next(cycle)
            nodes.append(new_node)
            if new_node in terminal_nodes:
                break
        return nodes

    @staticmethod
    def find_index_in_cycle(
        node_and_dir: tuple[str, str],
        cycle: tuple[int, list[tuple[str, str]]]
    ) -> int:
        return [elem[1] for elem in cycle].index(node_and_dir)

    def make_ghost_path(
        self,
        start_pattern: re.Pattern,
        direction: str,
        stop_pattern: re.Pattern,
    ) -> list[list[str]]:
        start_nodes = self.get_nodes_by_pattern(start_pattern)
        stop_nodes = self.get_nodes_by_pattern(stop_pattern)
        cycles = []
        for sn in start_nodes:
            cycles.append(self.make_cycle(sn, direction, stop_nodes))
        return cycles


network = GhostNetwork(nw)
path = network.make_ghost_path(
    re.compile(r"^.{2}A$"),
    inst,
    re.compile(r"^.{2}Z$"),
)

# It ought to be regarded as an accident of the design of this input that this
# works. In particular if XXA were included in each cycle, or if XXZ were not
# the last element of the cycle this would NOT. WORK.
ghost_path_length = math.lcm(*[len(c) - 1 for c in path])

print(ghost_path_length)
