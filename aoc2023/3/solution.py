import numpy
import numpy as np

# Get set of symbols
with open("puzzle_input", "r") as fo:
    CHAR_SET = set(fo.read())
    SYMBOL_SET = CHAR_SET - set("\n0123456789.")


# Gonna try to make this code a little bit nicer lol. For one thing, let's
# make the schematic an object
# Also, I quasi-committed to not using external libs but fuck that, i want numpy
class Schematic(np.ndarray):

    def get_neighbours(self, y, x) -> np.ndarray:
        # y --> line index in input, x --> char index in line
        y_min, y_max = max(0, y-1), min(len(self), y+2)
        x_min, x_max = max(0, x-1), min(len(self[y]), x+2)
        # Trusting the schematic is a rectangle ^
        return self[y_min:y_max,x_min:x_max]

    def get_val_at_pos(self, y, x) -> (int, int, int):
        if not self[y][x].isdigit():
            return 0, y, x
        start = x
        while start-1 > 0 and self[y][start-1].isdigit():
            start -= 1
        end = x
        while end < len(self[y]) and self[y][end].isdigit():
            end += 1
        return int("".join(self[y][start:end])), start, end

    def get_vals_at_neighbourhood(self, y, x) -> list[tuple[int, int, int]]:
        # INSCRUTABLE
        # return _non-zero_ vals in neighbourhood
        neighbour_vals = []
        for rel_idx, y_neigh in enumerate(self.get_neighbours(y, x), start=-1):
            if y_neigh[0].isdigit():
                neighbour_vals.append(self.get_val_at_pos(y+rel_idx, x-1))
                if (
                    len(y_neigh) == 3
                    and not y_neigh[1].isdigit()
                    and y_neigh[2].isdigit()
                    and x+1 < len(self[y+rel_idx])
                    and self.get_val_at_pos(y+rel_idx, x+1)[0]
                ):
                    neighbour_vals.append(self.get_val_at_pos(y+rel_idx, x+1))
                    continue
            else:
                if y_neigh[1].isdigit():
                    neighbour_vals.append(self.get_val_at_pos(y+rel_idx, x))
                elif len(y_neigh) == 3 and y_neigh[2].isdigit():
                    neighbour_vals.append(self.get_val_at_pos(y+rel_idx, x+1))
        return neighbour_vals

    def put_val_at_pos(self, val: str | int, y: int, x: int | tuple[int, int]) -> None:
        val = str(val)
        if isinstance(x, int) and len(val) == 1:
            self[y][x] = val
            return
        self[y, x[0]:x[1]] = list(val)

    def sum_array(self, y=None, x=None):
        if (y or x) is not None:
            return Schematic(self[y, x]).sum_array()
        numbers = []
        for y in range(0, len(self)):
            reading_num = False
            start = 0
            for x in range(0, len(self[y])):
                if self.get_val_at_pos(y, x)[0] > 0:
                    if not reading_num:
                        start = x
                        reading_num = True
                elif reading_num:
                    numbers.append(int("".join(self[y, start:x])))
                    reading_num = False
            if reading_num:
                numbers.append(int("".join(self[y, start:x+1])))
        return sum(numbers)


with open("puzzle_input", "r") as fo:
    schematic_array = np.array([list(line.strip()) for line in fo.readlines()]).view(Schematic)


numbers = np.full(schematic_array.shape, ".").view(Schematic)
# We'll go through the entire schematic, and find symbols in the neighbourhoods
# of digits.
for y in range(0, len(schematic_array)):
    for x in range(0, len(schematic_array[y])):
        if not schematic_array[y][x].isdigit():
            continue
        val, start, end = schematic_array.get_val_at_pos(y, x)
        is_adjacent = False
        for x0 in range(start, end):
            for neigh in schematic_array.get_neighbours(y, x0).flatten():
                if neigh in SYMBOL_SET:
                    numbers.put_val_at_pos(val, y, (start, end))
                    is_adjacent = True
                    break
            if is_adjacent:
                break

sum_numbers = numbers.sum_array()

print(sum_numbers)


# PART II

gear_complexes = []
for y in range(0, len(schematic_array)):
    for x in range(0, len(schematic_array[y])):
        if schematic_array[y][x] != "*":
            continue
        vals = schematic_array.get_vals_at_neighbourhood(y, x)
        if len(vals) == 2:
            gear_complexes.append(vals)

ratio = sum([g[0][0] * g[1][0] for g in gear_complexes])

print(ratio)
