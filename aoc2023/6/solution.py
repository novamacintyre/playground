import re

with open("puzzle_input", "r") as fo:
    puzzle_input = [line.strip() for line in fo.readlines()]

pattern = r"^(Time|Distance):\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)$"
times = [int(re.match(pattern, puzzle_input[0]).group(i)) for i in range(2, 6)]
distances = [int(re.match(pattern, puzzle_input[1]).group(i)) for i in range(2, 6)]


# We'll do things a little naiively at first, just looking for the min time
# holding the button
def get_time_to_distance(button_time, total_time):
    velocity = button_time
    distance = velocity * (total_time - button_time)
    return distance


# Get the fastest you can release the button
def get_lowest_record_time(total_time, record_distance):
    min_time = 0
    for i in range(1, total_time):
        if not i % 100_000:
            print(f"{i=},{total_time=}")
        if get_time_to_distance(i, total_time) > record_distance:
            min_time = i
            break
    return min_time


def get_num_record_times(total_time, record_distance):
    min_time = get_lowest_record_time(total_time, record_distance)
    if not min_time:
        return 0
    num_record_times = total_time - 2 * min_time + 1
    return num_record_times


num_times = []
for t, d in zip(times, distances):
    num_times.append(get_num_record_times(t, d))

prod_times = 1
for i in num_times:
    prod_times *= i

print(prod_times)


# PART II

times = int("".join([str(i) for i in times]))
distances = int("".join([str(i) for i in distances]))

# This is quite inefficient, mind you, but runs in a couple seconds on my
# machine. I was _planning_ to use a binary search, but evidently no need
record_times = get_num_record_times(times, distances)
print(record_times)
