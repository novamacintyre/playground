import copy

with open("puzzle_input", "r") as fo:
    puzzle_input = [line.strip() for line in fo.readlines()]


class Card:

    _card_to_value = {
        "A": 14,
        "K": 13,
        "Q": 12,
        "J": 11,
        "T": 10,
        **{str(i): i for i in range(2, 10)},
    }

    def __init__(self, card):
        self.card = card
        self._value = self._card_to_value[card]

    def __str__(self):
        return self.card

    def __int__(self):
        return self._value

    def __repr__(self):
        return self.__str__()

    def __hash__(self):
        return hash(repr(self))

    def __eq__(self, other):
        if isinstance(other, str):
            return self.card == other
        return self._value == int(other)

    def __ge__(self, other):
        if isinstance(other, str):
            return self._value >= self._card_to_value[other]
        return self._value >= int(other)

    def __gt__(self, other):
        return self.__ge__(other) and not self.__eq__(other)

    def __le__(self, other):
        return not self.__ge__(other) or self.__eq__(other)

    def __lt__(self, other):
        return not self.__ge__(other)

    def __ne__(self, other):
        return not self.__eq__(other)


class Hand:
    def __init__(self, cards, bid):
        self.bid = int(bid)
        self.cards = tuple(Card(c) for c in cards)

    @property
    def type_(self):
        if len(set(self.cards)) == 1:
            # Five of a kind
            return 7
        if len(set(self.cards)) == 2:
            if str(self).count(str(self.cards[0])) in (1, 4):
                # Four of a kind
                return 6
            # Full house
            return 5
        if len(set(self.cards)) == 5:
            # High card
            return 1
        if len(set(self.cards)) == 4:
            # One pair
            return 2
        unique_cards = set(self.cards)
        for card in unique_cards:
            if str(self).count(str(card)) == 3:
                # Three of a kind
                return 4
        # By elimination, two pair:
        return 3

    def __str__(self):
        return "".join([str(c) for c in self.cards])

    def __eq__(self, other):
        if self.type_ != other.type_:
            return False
        for idx, c in enumerate(self.cards):
            if c != other.cards[idx]:
                return False
        return True

    def __ge__(self, other):
        if self.__eq__(other):
            return True
        if self.type_ > other.type_:
            return True
        if self.type_ < other.type_:
            return False
        for idx, c in enumerate(self.cards):
            if c < other.cards[idx]:
                return False
            if c > other.cards[idx]:
                return True
        return False

    def __gt__(self, other):
        return self.__ge__(other) and not self.__eq__(other)

    def __le__(self, other):
        return not self.__ge__(other) or self.__eq__(other)

    def __lt__(self, other):
        return not self.__ge__(other)

    def __ne__(self, other):
        return not self.__eq__(other)


hands = []
for line in puzzle_input:
    hand, bid = line.split(" ")
    hands.append(Hand(hand, bid))

hands = sorted(hands)

total_winnings = 0
for idx, h in enumerate(hands):
    total_winnings += h.bid * (idx + 1)

print(total_winnings)


# PART II

class Part2Card(Card):

    _card_to_value = copy.copy(Card._card_to_value)
    _card_to_value["J"] = min(_card_to_value.values()) - 1


class Part2Hand(Hand):

    def __init__(self, cards, bid):
        super().__init__(cards, bid)
        self.cards = tuple(Part2Card(c) for c in cards)

    @property
    def type_(self):
        num_jokers = str(self).count("J")
        cards_no_jokers = str(self).replace("J", "")
        if len(set(cards_no_jokers)) in (0, 1):
            # Five of a kind
            return 7
        if len(set(cards_no_jokers)) == 2:
            first_card_count = str(cards_no_jokers).count(str(cards_no_jokers[0]))
            if (
                (
                    num_jokers == 0
                    and first_card_count in (1, 4)
                ) or (
                    num_jokers == 1
                    and first_card_count in (1, 3)
                )
                or (
                    num_jokers in (2, 3)
                )
            ):
                # Four of a kind
                return 6
            # Full house
            return 5
        if len(set(cards_no_jokers)) == 5:
            # High card, implied no jokers
            return 1
        if len(set(cards_no_jokers)) == 4:
            # One pair
            # (num_jokers == 0 | (num_jokers == 1 & non-jokers are unique))
            return 2
        unique_cards = set(cards_no_jokers)
        if len(unique_cards) == 3 and num_jokers in (1, 2):
            # Three of a kind
            return 4
        for card in unique_cards:
            if str(cards_no_jokers).count(str(card)) == 3:
                # Three of a kind
                return 4
        # By elimination, two pair:
        return 3


hands = []
for line in puzzle_input:
    hand, bid = line.split(" ")
    hands.append(Part2Hand(hand, bid))

hands = sorted(hands)

total_winnings = 0
for idx, h in enumerate(hands):
    total_winnings += h.bid * (idx + 1)

print(total_winnings)


