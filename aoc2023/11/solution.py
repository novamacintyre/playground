import copy

import numpy as np


with open("puzzle_input", "r") as fo:
    puzzle_input = fo.readlines()


class SpaceSegment:

    def __init__(self, raw_space: list[str] | np.ndarray, expansion_factor=2):
        self.expansion_factor = expansion_factor
        if isinstance(raw_space, np.ndarray):
            self.space = raw_space
        else:
            self.space = np.array([list(line.strip()) for line in raw_space])

    @classmethod
    def get_classical_expansion(cls, spc_seg):
        new_space = copy.deepcopy(spc_seg.space)
        y_ins = 0
        x_ins = 0
        for idy, row in enumerate(spc_seg.space):
            if "#" not in row:
                new_space = np.insert(
                    new_space, idy + x_ins, ".", axis=0,
                )
                x_ins += 1
            if "#" not in spc_seg.space[:, idy]:
                new_space = np.insert(
                    new_space, idy + y_ins, ".", axis=1,
                )
                y_ins += 1

        return cls(new_space)

    def distance(self, pt1: tuple[int, int], pt2: tuple[int, int]):
        d = 0
        for y in range(min(pt1[0], pt2[0]), max(pt1[0], pt2[0])):
            if "#" not in self.space[y, :]:
                d += self.expansion_factor
            else:
                d += 1
        for x in range(min(pt1[1], pt2[1]), max(pt1[1], pt2[1])):
            if "#" not in self.space[:, x]:
                d += self.expansion_factor
            else:
                d += 1
        return d

    @property
    def galaxies(self):
        gals = []
        for idy, row in enumerate(self.space):
            for idx, char in enumerate(row):
                if char == "#":
                    gals.append((idy, idx))
        return tuple(gals)


def generate_galaxy_pairs(space):
    gps = set()
    for gal1 in space.galaxies:
        for gal2 in space.galaxies:
            if gal1 != gal2 and (gal2, gal1) not in gps:
                gps.add((gal1, gal2))
    return gps


sp = SpaceSegment(puzzle_input)

gal_pals = generate_galaxy_pairs(sp)

distances = [sp.distance(gal1, gal2) for (gal1, gal2) in gal_pals]

sum_distances = sum(distances)

print(sum_distances)


# PART II
sp = SpaceSegment(puzzle_input, expansion_factor=1_000_000)

gal_pals = generate_galaxy_pairs(sp)

distances = [sp.distance(gal1, gal2) for (gal1, gal2) in gal_pals]

sum_distances = sum(distances)

print(sum_distances)
