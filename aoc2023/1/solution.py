# PART I
import re

with open("calibration_document", "r") as fo:
    cal_values = fo.readlines()


# I'm gonna do a nasty list comprehension :)
val_sum = sum([
    int(
        "".join([
            re.match(r".*?(\d)", cv).group(1),
            re.match(r".*?(\d)", cv[::-1].lstrip()).group(1),
        ])
    ) for cv in cal_values
])


print(val_sum)


# PART II
import re

with open("calibration_document", "r") as fo:
    cal_values = fo.readlines()

# https://gitlab.com/novamacintyre/playground/-/raw/main/aoc2023/1/f5d7e62833d7222b000464d06499e717ee00811c901d02a28e95b28899ebb8e6.jpg
a_regex = re.compile(r"(zero|one|two|three|four|five|six|seven|eight|nine|\d)")
b_regex = re.compile(r".*(zero|one|two|three|four|five|six|seven|eight|nine|\d)")
vals = sum([
    int(
        "".join([
            "0" if regex.search(cv).group(1) == "zero" else
            "1" if regex.search(cv).group(1) == "one" else
            "2" if regex.search(cv).group(1) == "two" else
            "3" if regex.search(cv).group(1) == "three" else
            "4" if regex.search(cv).group(1) == "four" else
            "5" if regex.search(cv).group(1) == "five" else
            "6" if regex.search(cv).group(1) == "six" else
            "7" if regex.search(cv).group(1) == "seven" else
            "8" if regex.search(cv).group(1) == "eight" else
            "9" if regex.search(cv).group(1) == "nine" else
            regex.search(cv).group(1)
            for regex in [a_regex, b_regex]
        ])
    )
    for cv in cal_values
])

print(val_sum)
