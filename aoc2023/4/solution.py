# PART I
import math
import re
import queue

with open("puzzle_input", "r") as fo:
    raw_cards = [line.strip() for line in fo.readlines()]

# Ugly re, whoops
card_re = re.compile(r"Card\s+(\d+): ((\d+\s*?)+)\s*?\|\s+((\d+\s*?)+)$")

points = 0
for card in raw_cards:
    card = " ".join(card.split())
    match = card_re.match(card)
    winning_nums_str, my_nums_str = match.group(2), match.group(4)
    winning_nums = set(winning_nums_str.split(" "))
    my_nums = set(my_nums_str.split(" "))
    intersection = my_nums & winning_nums
    # If len(intersection) == 0, then score == 0.5, so we just round down
    score = math.floor(2 ** (len(intersection) - 1))
    points += score

print(points)


# PART II

cards = {}
for rc in raw_cards:
    rc = " ".join(rc.split())
    match = card_re.match(rc)
    winning_nums_str, my_nums_str = match.group(2), match.group(4)
    winning_nums = set(winning_nums_str.split(" "))
    my_nums = set(my_nums_str.split(" "))
    intersection = my_nums & winning_nums
    cards[match.group(1)] = {
        "my_nums": my_nums,
        "winning_nums": winning_nums,
        "intersection": intersection,
        "points": len(intersection),
    }

# There is an exponential blowup here. It's rough. Would be better to go
# backwards and compute the effect of every card to the total number of new
# cards
card_queue = queue.SimpleQueue()
for ckey, cval in cards.items():
    card_queue.put((ckey, cval))

cards_processed = 0
while not card_queue.empty():
    ckey, cval = card_queue.get()
    for i in range(int(ckey)+1, int(ckey)+cval["points"]+1):
        card_queue.put((str(i), cards[str(i)]))
    cards_processed += 1
    if cards_processed % 100000 == 0:
        print(f"{ckey=},{cards_processed=},{card_queue.qsize()}")

print(cards_processed)
