with open("puzzle_input", "r") as fo:
    puzzle_input = [line.strip() for line in fo.readlines()]


def get_next_value(measurements):
    derivs = [measurements]
    while any([d != 0 for d in derivs[-1]]):
        new_derivs = []
        for i in range(0, len(derivs[-1]) - 1):
            new_derivs.append(derivs[-1][i+1] - derivs[-1][i])
        derivs.append(new_derivs)
    next_increase = 0
    for i in range(len(derivs)-1, -1, -1):
        next_increase += derivs[i][-1]
    return next_increase


next_values = []
for l in puzzle_input:
    measurements = [int(i) for i in l.split()]
    next_values.append(get_next_value(measurements))

print(sum(next_values))


# PART II
# Do the same just in reverse
next_values = []
for l in puzzle_input:
    measurements = [int(i) for i in l.split()]
    measurements.reverse()
    next_values.append(get_next_value(measurements))

print(sum(next_values))
