import math
import re

# Parse the cube
cube_re = re.compile(r"(\d+) (red|green|blue)([,;]?)")

with open("puzzle_input", "r") as fo:
    games = fo.readlines()

game_draws = []
for g in games:
    game_draws.append([])
    matches = cube_re.findall(g)
    new_draw = {}
    for m in matches:
        new_draw[m[1]] = int(m[0])
        if not m[2] or m[2] == ";":
            game_draws[-1].append(
                new_draw
            )
            # Pretty sure this doesn't overwrite new_draw in game_draws but need to check
            new_draw = {}

# Eliminate impossibilities
MAX_CUBES = {
    "red": 12,
    "green": 13,
    "blue": 14,
}

id_sum = 0
ids = []
for idx, draws in enumerate(game_draws, 1):
    impossible = False
    for d in draws:
        for colour, number in d.items():
            if number > MAX_CUBES[colour]:
                impossible = True
                break
        if impossible:
            break
    if impossible:
        continue
    id_sum += idx
    ids.append(idx)

print(id_sum)

# PART II

# Determine the MVC (Minimum Viable Cubes)
mvc = []
for draws in game_draws:
    # Defaulting to 1 is weird, but we don't want to zero out the product
    min_cubes = {
        "red": 1,
        "green": 1,
        "blue": 1
    }
    for d in draws:
        min_cubes.update(
            {
                colour: d[colour]
                if d[colour] > min_cubes[colour]
                else min_cubes[colour]
                for colour in d.keys()
            }
        )
    mvc.append(min_cubes)

prod_sum = sum([
    math.prod(g.values()) for g in mvc
])

print(prod_sum)
